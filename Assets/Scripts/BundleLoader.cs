﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;

public class BundleLoader : MonoBehaviour
{
	private const float Y = 0.347f;
	private const float LEFT_X = -0.95f;
	private const float RIGHT_X = 0.95f;
	private const float BACK_Z = -1.345f;
	private const float FRONT_Z = 1.37f;

	private Vector3 _leftBackLocation = new Vector3 (LEFT_X, Y, BACK_Z);
	private Vector3 _leftFrontLocation = new Vector3 (LEFT_X, Y, FRONT_Z);
	private Vector3 _rightBackLocation = new Vector3 (RIGHT_X, Y, BACK_Z);
	private Vector3 _rightFrontLocation = new Vector3 (RIGHT_X, Y, FRONT_Z);

	private int _currentIndex = 0;

	private List<GameObject> _wheels = new List<GameObject> ();

	private List<string> _resources = new List<string> () {
		"right_front_wheel",
		"right_back_wheel",
		"left_front_wheel",
		"left_back_wheel"
	};

	public void LoadBundles ()
	{
		foreach (GameObject wheel in _wheels) {
			Destroy (wheel);
		}
		if (_currentIndex == _resources.Count - 1) {
			_currentIndex = 0;
		} else {
			_currentIndex++;
		}
		Object wheelSource = Resources.Load (_resources [_currentIndex]);
		_wheels.Add (InstantiateWheel (wheelSource, _leftBackLocation));
		_wheels.Add (InstantiateWheel (wheelSource, _leftFrontLocation));
		_wheels.Add (InstantiateWheel (wheelSource, _rightBackLocation));
		_wheels.Add (InstantiateWheel (wheelSource, _rightFrontLocation));
	}

	private GameObject InstantiateWheel (Object instance, Vector3 position)
	{
		GameObject currentObject = Instantiate (instance) as GameObject;
		if (currentObject != null) {
			currentObject.transform.position = position;
		}
		return currentObject;
	}
}
