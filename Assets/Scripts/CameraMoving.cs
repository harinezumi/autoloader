﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMoving : MonoBehaviour
{
	[SerializeField]
	private Camera _camera;
	
	private CameraHandler _cameraHandler;
	// Use this for initialization
	void Start ()
	{
		_cameraHandler = new CameraHandler (_camera);
	}
	
	// Update is called once per frame
	void Update ()
	{
		ZoomInfo info = _cameraHandler.DetectZoom ();
		if (info != null)
			_cameraHandler.HandleZoom (info);
		RotationInfo rotation = _cameraHandler.DetectRotation ();
		if (rotation != null)
			_cameraHandler.HandleRotation (rotation);
	}
}
