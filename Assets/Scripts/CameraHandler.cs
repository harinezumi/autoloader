﻿using System;
using UnityEngine;

public class CameraHandler
{
	protected Camera _camera;
	private Transform _cameraTransform;
	private const float ROTATION_SPEED = 5;
	private const float ZOOM_SPEED = 5;
	private const int MAX_ZOOM = 90;
	private const int MIN_ZOOM = 15;
	private const float MIN_AROUND_X_ROTATION = -45;
	private const float MAX_AROUND_X_ROTATION = 45;
	private const float CIRCLE = 360;

	public CameraHandler ()
	{
	}

	public CameraHandler (UnityEngine.Camera camera)
	{
		_camera = camera;
		_cameraTransform = camera.transform;
	}

	public void HandleRotation (RotationInfo rotationInfo)
	{
		if (rotationInfo.amount.x != 0) {
			int sign = rotationInfo.amount.x > 0 ? 1 : -1;
			_cameraTransform.RotateAround (new Vector3 (), Vector3.up, ROTATION_SPEED * sign);
			return;
		}
		if (rotationInfo.amount.y != 0) {
			int sign = rotationInfo.amount.y > 0 ? -1 : 1;
			float currentRotationX = _cameraTransform.rotation.eulerAngles.x;
			if (currentRotationX > 0.5f * CIRCLE) {
				currentRotationX = currentRotationX - CIRCLE;
			}
			float currentRotationY = _cameraTransform.rotation.eulerAngles.y;
			if (currentRotationY > 0.5f * CIRCLE) {
				currentRotationY = currentRotationY - CIRCLE;
			}
			if (sign == 1) {
				if (currentRotationX >= MAX_AROUND_X_ROTATION)
					return;
			} else {
				if (currentRotationX <= MIN_AROUND_X_ROTATION)
					return;
			}
			Vector3 rotated = Quaternion.Euler (0, currentRotationY, 0) * Vector3.right;
			_cameraTransform.RotateAround (new Vector3 (), rotated, ROTATION_SPEED * sign);
		}
	}

	public void HandleZoom (ZoomInfo zoomInfo)
	{
		float newFov = _camera.fieldOfView;
		if (zoomInfo.isZoomIn)
			newFov += ZOOM_SPEED;
		else
			newFov -= ZOOM_SPEED;
		_camera.fieldOfView = Mathf.Clamp (newFov, MIN_ZOOM, MAX_ZOOM);
	}

	public ZoomInfo DetectZoom ()
	{
		ZoomInfo info = new ZoomInfo ();
		if (Input.GetAxis ("Mouse ScrollWheel") < 0) {
			info.isZoomIn = true;
			return info;
		}
		if (Input.GetAxis ("Mouse ScrollWheel") > 0) {
			info.isZoomIn = false;
			return info;
		}
		return null;
	}

	public RotationInfo DetectRotation ()
	{
		RotationInfo info = new RotationInfo ();
		info.amount = new Vector2 ();
		if (Input.GetMouseButton (0)) {
			info.amount.x = Input.GetAxis ("Mouse X");
			info.amount.y = Input.GetAxis ("Mouse Y");
			if (info.amount.x != 0 || info.amount.y != 0) {
				return info;
			}
		}
		return null;
	}
}

